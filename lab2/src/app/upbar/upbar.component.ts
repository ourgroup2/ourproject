import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-upbar',
  templateUrl: './upbar.component.html',
  styleUrls: ['./upbar.component.scss']
})
export class UpbarComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
  }



tableclick()
{
  console.log("Table Page");
  this.router.navigate(['table']);
}

uploadclick()
{
  console.log("Upload Page");
  this.router.navigate(['upload']);
}

logout() {
  this.auth.logout();
  console.log("Login Page");
  this.router.navigate(['']);
}

}
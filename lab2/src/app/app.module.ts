import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { UploadComponent } from './upload/upload.component';
import { TableComponent } from './table/table.component';
import { MatTable, MatTableModule } from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox'

import { FormsModule } from '@angular/forms';
import { RoutingModule } from 'angular-routing';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import { UpbarComponent } from './upbar/upbar.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';

import {ReactiveFormsModule} from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './service/auth.service';
import { AuthGuard } from './service/auth.guard';
import { InformationsComponent } from './informations/informations.component';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UploadComponent,
    TableComponent,
    UpbarComponent,
    RegisterComponent,
    InformationsComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    RoutingModule.forRoot(),
    MatButtonToggleModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['http://localhost:3000'],
        disallowedRoutes: ['http://localhost:3000/auth']
      }
    })
  ],
  providers: [
    AuthService,
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileUploadServiceService {

  constructor(private http: HttpClient) { }

  postFile(fileToUpload: File): Observable<Object> {
    const endpoint = 'http://localhost:3000/evaluate';
    const formData: FormData = new FormData();
    formData.append('img', fileToUpload, fileToUpload.name);
    console.log('postFile function');
    return this.http.post(endpoint, formData).pipe(take(1), tap((resp: any) => {
      console.log(resp);
  }))

}

refreshHeaders(){
  let headers = new HttpHeaders().set('Content-Type', 'application/json')
  headers.set("Access-Control-Allow-Origin",'*');
  headers.set('Accept', 'application/json');
  headers.set("Access-Control-Allow-Methods", "GET , PUT , POST , DELETE");

  
  const userToken =  localStorage.getItem('access_token');
  
  if(userToken)
    headers.set('Authorization', userToken );

  return headers
}
}

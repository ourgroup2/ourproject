import { Injectable } from '@angular/core';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderStateService {

  events = new EventEmitter<boolean>();

  constructor() { }

  show() {
    this.events.emit(true);
  }

  hide() {
    this.events.emit(false);
  }
}

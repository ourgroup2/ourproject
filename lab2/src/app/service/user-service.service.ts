import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RestRequestService } from './rest-request.service';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  userData;
  constructor(private restService: RestRequestService) { }

  register(user): Observable<any> {
    return this.restService.post('users', user).pipe(tap(data => {
      this.userData = data;
    }))
  }
}

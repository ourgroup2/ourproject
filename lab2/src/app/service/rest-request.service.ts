import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestRequestService {

  constructor(private http: HttpClient) { }
  get(url) {
    
    return this.http.get("http://localhost:3000/" + url, {headers : this.refreshHeaders()});
  }

  public post(url, body) : Observable<any> {
    return this.http.post("http://localhost:3000/" + url, body, {headers : this.refreshHeaders()}).pipe(take(1));
  }

  refreshHeaders(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    headers.set("Access-Control-Allow-Origin",'*');
    headers.set('Accept', 'application/json');
    headers.set("Access-Control-Allow-Methods", "GET , PUT , POST , DELETE");

    
    const userToken =  localStorage.getItem('access_token');
    
    if(userToken)
      headers.set('Authorization', userToken );

    return headers
  }
}

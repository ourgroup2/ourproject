import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-informations',
  templateUrl: './informations.component.html',
  styleUrls: ['./informations.component.scss']
})
export class InformationsComponent implements OnInit {

  info: any

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get("https://us-central1-our-project-fsd.cloudfunctions.net/helloWorld").subscribe({
      next: (information: any) => { this.info = JSON.stringify(information[0]); }
    });

  }
}

import { PercentPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import{SelectionModel} from '@angular/cdk/collections';
import { HeaderStateService } from '../service/header-state.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RestRequestService } from '../service/rest-request.service';
import { HttpClient } from '@angular/common/http';


export interface Image {
  imageName: string;
  size: number;
  recognitionResult: string;
  imageDownloadLink: string;
}


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit {

  images: Image[];
  displayedColumns: string[] = ['select','imageName','size','recognitionResult','link'];
  Source=new MatTableDataSource<Image>();
  
  selection = new SelectionModel<Image>(true, []);

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.Source.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.Source.data.forEach(row => this.selection.select(row));
  }


  constructor(private headerService:HeaderStateService,private restService: RestRequestService) {
    this.headerService.show();

    this.restService.get('history')
    .subscribe((data:any)=>{
      this.Source.data=data;
    });
  }


  ngOnInit(): void {

     
  }

}

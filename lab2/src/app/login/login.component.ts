import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HeaderStateService } from '../service/header-state.service';
import { UserServiceService } from '../service/user-service.service';
import { AuthService } from '../service/auth.service';
import {  take } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public error: string;

  loginForm: FormGroup;
  submitted = false;

  constructor(private router: Router,private formBuilder: FormBuilder,private headService:HeaderStateService, private auth: AuthService) { 
    this.headService.hide();
  }

  get f(){
    return this.loginForm.controls;
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
  }
  
  onSubmit(){
    this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.auth.login(this.loginForm.controls.email.value.toString() , this.loginForm.controls.password.value.toString())
        .pipe(take(1))
        .subscribe(
          success => this.router.navigate(['table']),
          err => this.error = 'Email address or password is incorect!'
        );
    }
        //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))
  }



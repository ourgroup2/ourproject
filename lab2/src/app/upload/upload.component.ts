import { Component, OnInit } from '@angular/core';
import { HeaderStateService } from '../service/header-state.service';
import {FileUploadServiceService} from '../service/file-upload-service.service'

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  constructor(private headerService:HeaderStateService, private fileUploadService : FileUploadServiceService) {
    this.headerService.show();
   }

  ngOnInit(): void {
  }
  
  url: any;
  fileToUpload : any;
  onSelectFile(event) {

    this.fileToUpload = event.target.files.item(0) as File;
    console.log(this.fileToUpload)

      this.fileUploadService.postFile(this.fileToUpload).subscribe(data => {
          console.log('Success');
        }, error => {
          console.log(error);
        });


    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url


      reader.onload = (event) => { // called once readAsDataURL is completed
          this.url = event?.target?.result;
      }
    }
  }

}



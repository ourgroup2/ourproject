import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TableComponent } from './table/table.component';
import { UploadComponent } from './upload/upload.component';
import { AuthGuard } from './service/auth.guard';
import { InformationsComponent } from './informations/informations.component';

const routes: Routes = [
  {
    path: '', component: LoginComponent
  },
  {
    path: 'table', component: TableComponent, canActivate: [AuthGuard]
  },
  {
    path: 'upload', component: UploadComponent, canActivate: [AuthGuard]
  },
  {
    path: 'register', component: RegisterComponent
  },
  {
    path: 'informations', component: InformationsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import { HeaderStateService } from './service/header-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'lab2';
  
  show = false;

  subs;
  
    constructor(private headerService: HeaderStateService) {
  this.subs = this.headerService.events.subscribe(data => {
    this.show = data;
  })
  
    }
  
    ngOnDestroy() {
      if(this.subs) {
        this.subs.unsubscribe();
      }
    }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HeaderStateService } from '../service/header-state.service';
import { UserServiceService } from '../service/user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(private router: Router,private formBuilder: FormBuilder,private headService:HeaderStateService,
    private userService:UserServiceService) { 
    this.headService.hide();
  }

  get f(){
    return this.registerForm.controls;
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
  });
  }
  
  onSubmit(){
    this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        this.userService.register(this.registerForm.getRawValue()).subscribe(success => {
          this.router.navigate(['']);
          //show() header
        } , err => {
          alert(err.message);
        })
  
      }
        //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
  }


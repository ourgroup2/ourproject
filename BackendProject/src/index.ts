import "reflect-metadata";
import {createConnection, getRepository} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {User} from "./entity/User";
import { History } from "./entity/History";
import {MnistData} from './data.js';
import * as fileUpload from "express-fileupload"
createConnection().then(async connection => {
    
    const jwt = require('jsonwebtoken');
    const expressJwt = require('express-jwt');

    // create express app
    const app = express();
    app.use(bodyParser.json());

    app.use(fileUpload({
        useTempFiles : false,
        tempFileDir : '/tmp/'
    }));

    app.use(function (req, res, next) {
        //Enabling CORS
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
          next();
        });

        app.post('/auth', async function(req : Request, res: Response ) {
            const userRepository = getRepository(User);
            const users = await userRepository.find();
            var currentUserId = null;
            var currentUserEmail = null;
            var currentUserPassword = null;

            for (var user of users) {       
                if(user.email.match(req.body.email.toString()) !== null && user.password.match(req.body.password.toString()) !== null)
                {
                    currentUserId = user.id;
                    currentUserEmail = user.email;
                    currentUserPassword = user.password;
                }
            }

            if(currentUserEmail == null) return res.sendStatus(401);
    
                var token = jwt.sign({userID: userRepository.getId(currentUserId)}, 'our_app', {expiresIn: '2h'});
                res.send({token});
              });

    // register express routes from defined application routes
    Routes.forEach(route => {
        if(route.guard){
            (app as any)[route.method](route.route, [route.guard], (req: Request, res: Response, next: Function) => {
                const result = (new (route.controller as any))[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
    
                } else if (result !== null && result !== undefined) {
                    res.json(result);
                }
            });
        } else { 
            (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
                const result = (new (route.controller as any))[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
    
                } else if (result !== null && result !== undefined) {
                    res.json(result);
                }
            });
        }
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    await connection.manager.save(connection.manager.create(User, {
        firstName: "Timber",
        lastName: "Saw",
        email: "timber@gmail.com",
        password: "password"
    }));
    await connection.manager.save(connection.manager.create(User, {
        firstName: "Phantom",
        lastName: "Assassin",
        email: "phantom@gmail.com",
        password: "password"
    }));


    await connection.manager.save(connection.manager.create(History,{
        imageName:'Pineapple Pizza',
        size:400,
        recognitionResult:'Pizza',
        imageDownloadLink:'https://en.wikipedia.org/wiki/Hawaiian_pizza#/media/File:Hawaiian_pizza_1.jpg',
    }));


    await connection.manager.save(connection.manager.create(History,{
        imageName:'Pudding',
        size:300,
        recognitionResult:'Pudding',
        imageDownloadLink:'https://en.wikipedia.org/wiki/Pudding#/media/File:Pudim_Abade_de_Priscos.png',
    }));


    await connection.manager.save(connection.manager.create(History,{
        imageName:'Carbonara',
        size:450,
        recognitionResult:'Pasta',
        imageDownloadLink:'https://www.jocooks.com/wp-content/uploads/2012/05/creamy-carbonara-1.jpg',
    }));


    await connection.manager.save(connection.manager.create(History,{
        imageName:'Penguin',
        size:500,
        recognitionResult:'Penguin',
        imageDownloadLink:'https://ro.wikipedia.org/wiki/Pinguin#/media/Fi%C8%99ier:Pygoscelis_papua.jpg',
    }));


    await connection.manager.save(connection.manager.create(History,{
        imageName:'Turtle',
        size:200,
        recognitionResult:'Turtle',
        imageDownloadLink:'https://cdn.britannica.com/s:800x450,c:crop/66/195966-138-F9E7A828/facts-turtles.jpg',
    }));


    await connection.manager.save(connection.manager.create(History,{
        imageName:'Cockatiel',
        size:600,
        recognitionResult:'Parrot',
        imageDownloadLink:'https://lafeber.com/pet-birds/wp-content/uploads/2018/06/Cockatiel-2.jpg',
    }));

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));


function doPrediction(model, data, testDataSize = 1) {
  //console.log(data);
  const IMAGE_WIDTH = 28;
  const IMAGE_HEIGHT = 28;

  const testxs = data.reshape([1, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
  console.log(testxs);
  const labels = null;
  const preds = model.predict(testxs).argMax(-1);

  testxs.dispose();
  return [preds, labels];
}

module.exports.doPrediction = doPrediction;
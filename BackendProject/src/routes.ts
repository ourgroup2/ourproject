import { LogoController } from "./controller/LogoController";
import {UserController} from "./controller/UserController";
import {HistoryController} from "./controller/HistoryController";
import {EvaluateController} from "./controller/EvaluateController";
import {checkJWT} from "./checkJWT"

export const Routes = [
      {
        method: "post",
        route: "/evaluate",
        controller: EvaluateController,
        action: "evaluate",
        //guard: checkJWT
    },{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    //guard: checkJWT
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    guard: checkJWT
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    guard: checkJWT
},

{
    method: "get",
    route: "/logo",
    controller: LogoController,
    action: "all",
    guard: checkJWT
}, {
    method: "get",
    route: "/logo/:id",
    controller: LogoController,
    action: "one",
    guard: checkJWT
}, {
    method: "post",
    route: "/logo",
    controller: LogoController,
    action: "save",
    guard: checkJWT
}, {
    method: "delete",
    route: "/logo/:id",
    controller: LogoController,
    action: "remove",
    guard: checkJWT
},

{
    method: "get",
    route: "/history",
    controller: HistoryController,
    action: "all",
    //guard: checkJWT
}, {
    method: "get",
    route: "/history/:id",
    controller: HistoryController,
    action: "one",
    //guard: checkJWT
}, {
    method: "post",
    route: "/history",
    controller: HistoryController,
    action: "save",
    //guard: checkJWT
}, {
    method: "delete",
    route: "/history/:id",
    controller: HistoryController,
    action: "remove",
    //guard: checkJWT
}];
import {NextFunction, Request, Response} from "express";
import {MnistData} from  '../data';
import * as tf from "@tensorflow/tfjs-node";
import {doPrediction} from '../script'
import {getRepository} from "typeorm";
import { History } from "../entity/History";
import {Routes} from "../routes";
import { HistoryController } from "./HistoryController";


export class EvaluateController {

  private historyRepository = getRepository(History);

    async evaluate(request: Request, response: Response, next: NextFunction) {  
        const predValues = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"]
        console.log(request.files.img);
        //const data = new MnistData();
        //await data.load(request.files.img);
        
        const data = tf.node.decodeImage(request.files.img.data);
        const dataGray = data.mean(2);
        console.log(dataGray);
        const model = await tf.loadLayersModel('file://src/assets/model.json');
        if(model != null)
        {
          //model.summary();
        }
        //console.log(data.datasetImages);
        
        const [preds, labels] = doPrediction(model, dataGray, 1);
        //console.log(preds.toString())
        var pred = Number(preds.toString()[12])
        const recordData ={  imageName: request.files.img.name,
        size: request.files.img.size,
        recognitionResult: predValues[pred],
        imageDownloadLink: request.files.img.name,
      };
        await this.historyRepository.save(recordData);

        
        //response.send(prediction.toString())

      }
}
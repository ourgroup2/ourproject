import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {History} from "../entity/History";

export class HistoryController {

    private historyRepository = getRepository(History);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.historyRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.historyRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.historyRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let historyToRemove = await this.historyRepository.findOne(request.params.id);
        await this.historyRepository.remove(historyToRemove);
    }

}
import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Logo} from "../entity/Logo";

export class LogoController {

    private logoRepository = getRepository(Logo);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.logoRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.logoRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.logoRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let logoToRemove = await this.logoRepository.findOne(request.params.id);
        await this.logoRepository.remove(logoToRemove);
    }

}
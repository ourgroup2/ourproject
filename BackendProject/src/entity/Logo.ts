import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Logo {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    logoImage: string;

}

import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class History {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    imageName: string;
      
    @Column()
    size: number;

    @Column()
    recognitionResult: string;

    @Column()
    imageDownloadLink: string;

}

import  {Datastore} from '@google-cloud/datastore';

const datastore = new Datastore({
  projectId: 'our-project-fsd',
  keyFilename: 'our-project-fsd-246b5212f326.json'
});

export function helloWorld(req, res) {
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'GET')
  res.set('Access-Control-Allow-Headers', "*");
  const query = datastore.createQuery('message');
  const logs = datastore.runQuery(query).then(entity => {
    console.log(entity);
    res.status(200).send(entity);
  }).catch(err => {
    console.error('ERROR:', err);
    res.status(200).send(err);
    return;
  });

};

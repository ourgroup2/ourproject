var Datastore = require('@google-cloud/datastore').Datastore

var datastore = new Datastore({
    projectId: 'our-project-fsd',
    keyFilename: 'our-project-fsd-246b5212f326.json'
});
function helloWorld(req, res) {
    res.set('Access-Control-Allow-Origin', "*")
    res.set('Access-Control-Allow-Methods', 'GET')
    res.set('Access-Control-Allow-Headers', "*");
    var query = datastore.createQuery('message');
    var logs = datastore.runQuery(query).then(function (entity) {
        console.log(entity);
        res.status(200).send(entity);
    }).catch(function (err) {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
    });
}

exports.helloWorld = helloWorld;

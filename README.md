<h1> Our Project </h1>
<h3> Frontend </h3>
<ul>
	<li>ng add @angular/material</li>
	<li>npm install angular-routing</li>
	<li>npm install @auth0/angular-jwt</li>
</ul>

<h3> Backend </h3>
<ul>
	<li>npm install body-parser jsonwebtoken express-jwt</li>
	<li>npm install npm@latest -g </li>
	<li>npm i @tensorflow/tfjs-node</li>
        <li>npm i isomorphic-fetch</li>
	<li>npm install --save express-fileupload</li>
</ul>

<h3> Our Cloud </h3>
<ul>
	<li>npm install @google-cloud/functions-framework</li>
	<li>npm install -g tsc</li>
	<li>npm install cors</li>
	<li>npm install @google-cloud/datastore</li>
</ul>
